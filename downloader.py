import os, codecs, subprocess, time, datetime, csv, shutil
from os.path import join
from os import path

os.chdir(os.path.dirname(os.path.realpath(__file__)))
fileDir =  os.path.dirname(os.path.realpath(__file__))

token = ''
with open('token.txt', 'r') as f:
    token = f.readline()
exePath = join(fileDir, 'DiscordChatExporter.CLI\\DiscordChatExporter.Cli.exe')
downloadPath = join(fileDir, 'downloads')
cachePath = join(fileDir, 'cache')
FNULL = open(os.devnull, 'w')


serverID = '177192169516302336'
announcements = '487288519123795968'
discussion = '487285437086433292'
myServer = '447708037902368780'
channel = myServer

serverList = [myServer, discussion, announcements]

# Clearing cache
if path.isdir(cachePath):
    shutil.rmtree(cachePath)
os.mkdir(cachePath)

extensionList = {'HtmlDark':'html', 'HtmlLight':'html', 'Csv':'csv', 'PlainText':'txt'}

def getPastTime(delay):
    now = datetime.datetime.now()
    oldTime = now - delay

    # Stupid offset because of windows time
    # Changes often
    offset = datetime.timedelta(hours=2)
    oldTime = oldTime - offset

    result = oldTime.strftime('%d-%b-%y %I:%M %p')
    return result

def downloadChannel(token, channel, fileFormat, directory, after):
    # Check if metadata file exists
    if not os.path.exists(join(directory, 'meta.txt')):
        with codecs.open(join(directory, 'meta.txt'), 'w', encoding='utf-8'):
            None

    extension = extensionList.get(fileFormat)

    downloadList = os.listdir(downloadPath)
    # Removing earlier downloads of the same type
    for filename in downloadList:
        if channel in filename and filename[-len(extension):] == extension:
            os.remove(join(directory, filename))

    filesBefore = os.listdir(directory)

    startTime = time.time()
    out = subprocess.check_output([exePath, 'export', '-t', token, '-c', channel, '-o', directory, '-f', fileFormat, '--after', after], stderr=subprocess.STDOUT, universal_newlines=True)
    print(out)
    print('Download time: {}s'.format(time.time() - startTime))

    # Connects the filename with the channel ID because it is not done by default
    filesAfter = os.listdir(directory)
    # Finds the new item and calls it the downloaded file. NOTE: only works when the system is not tampered with
    filename = ''
    for downloadFile in filesAfter:
        if downloadFile not in filesBefore:
            filename = downloadFile
            break
    
    # Writes the file info to the metadata file
    with codecs.open(join(directory, 'meta.txt'), 'a', encoding='utf-8') as f:
        f.write('{};{}\n'.format(channel, filename))


# def readChannel(channel, directory, fileFormat):
#     extension = extensionList.get(fileFormat)

#     downloadList = os.listdir(downloadPath)
#     downloadedFile = ''
#     for filename in downloadList:
#         if channel in filename and filename[-len(extension):] == extension:
#             downloadedFile = filename
#             break
#     downloadedFilePath = join(directory, downloadedFile)

#     # testing opening as a csv file
#     messages = []
#     with codecs.open(downloadedFilePath, 'r', encoding='utf-8') as csvFile:
#         reader = csv.reader(csvFile, delimiter = ';')
#         for row in reader:
#             # print(row)
#             messages.append(row)
#             if row[4] != '':
#                 print(row)
#     messages = messages[1:]
#     return messages


def downloadPass(downloadPath, cachePath, saveDownloads):
    if len(os.listdir(downloadPath)) == 0:
        print('Error - Empty Download Directory!')
        return

    # Deleting the cache
    if path.isdir(cachePath):
        shutil.rmtree(cachePath)
    os.mkdir(cachePath)
    print('Deleted cache')
    
    downloadFiles = os.listdir(downloadPath)
    for filename in downloadFiles:
        if saveDownloads: # Copying the files
            shutil.copyfile(join(downloadPath, filename), join(cachePath, filename))
        else: #Moving the files    
            os.rename(join(downloadPath, filename), join(cachePath, filename))
    if saveDownloads:
        print('Copied {} downloaded file(s) to cache'.format(len(downloadFiles)))
    else:
        print('Moved {} downloaded file(s) to cache'.format(len(downloadFiles)))
    
    # Downloading all the servers
    for server in serverList:
        downloadChannel(token, server, 'Csv', downloadPath, oldTime)


delay = datetime.timedelta(days=7, hours=0, minutes=0)
oldTime = getPastTime(delay)

# downloadChannel(token, channel, 'Csv', downloadPath, oldTime)

# New idea: At the start of every pass, delete cache and replace with downloads

downloadPass(downloadPath, cachePath, False)

print()