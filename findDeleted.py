import os, codecs, subprocess, time, datetime, csv, shutil
from os.path import join
from os import path

os.chdir(os.path.dirname(os.path.realpath(__file__)))
fileDir =  os.path.dirname(os.path.realpath(__file__))


downloadPath = join(fileDir, 'downloads')
cachePath = join(fileDir, 'cache')

def loadMessages(filename):
    print('Reading {}'.format(filename))
    messages = []
    with codecs.open(filename, 'r', encoding='utf-8') as csvFile:
        reader = csv.reader(csvFile, delimiter = ';')
        for row in reader:
            # print(row)
            messages.append(row)
            if row[4] != '':
                print(row)
    messages = messages[1:]
    return messages

def findCommonFiles(downloadPath, cachePath):
    # Finds the filenames for files present in both the download folder and the cache
    # Compensates for different filenames from date
    downloadFiles = [] # One element has format [id, filename]
    with codecs.open(join(downloadPath, 'meta.txt'), 'r', encoding='utf-8') as csvFile:
        reader = csv.reader(csvFile, delimiter=';')
        for row in reader:
            print([row])
            downloadFiles.append(row)

    cacheFilenames = [] # Seperate lists unlike downloadFiles
    cacheIDs = []
    with codecs.open(join(cachePath, 'meta.txt'), 'r', encoding='utf-8') as csvFile:
        reader = csv.reader(csvFile, delimiter=';')
        for row in reader:
            # print([row])
            cacheIDs.append(row[0])
            cacheFilenames.append(row[1])
    
    dFiles = [] # Download filenames
    cFiles = [] # Cache filenames
    for i in range(len(downloadFiles)):
        if downloadFiles[i][0] in cacheIDs:
            # File in cache is also in download
            index = cacheIDs.index(downloadFiles[i][0]) # Finding the position of the matching ID
            dFiles.append(downloadFiles[i][1]) # Find the corresponding filename for the ID and form a list
            cFiles.append(cacheFilenames[index])
    print(dFiles)
    print(cFiles)
    return dFiles, cFiles

def compareMessages(downloadMessages, cacheMessages):
    # Finds all messages in cache that have been edited or deleted

    userMessages = {} # Keeps track of users and their messages in a nested dictionary
    # Format: {user : {time : [messagesMadeDuringThatTime]}}
    for message in downloadMessages:
        author = message[0]
        if author not in userList:
            userList.append(author)
    

    return None

downloadFiles, cacheFiles = findCommonFiles(downloadPath, cachePath)

downloadMessages = loadMessages(join(downloadPath, downloadFiles[1]))
cacheMessages = loadMessages(join(cachePath, cacheFiles[1]))

compareMessages(downloadMessages, cacheMessages)

print('Done!')