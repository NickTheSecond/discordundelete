import os, codecs, subprocess, time, datetime, csv, shutil
from os.path import join
from os import path

os.chdir(os.path.dirname(os.path.realpath(__file__)))
fileDir =  os.path.dirname(os.path.realpath(__file__))

downloadPath = join(fileDir, 'downloads')
cachePath = join(fileDir, 'cache')

downloadList = os.listdir(downloadPath)

downloadedFilePath = join(downloadPath, downloadList[0])

messages = []
with codecs.open(downloadedFilePath, 'r', encoding='utf-8') as csvFile:
    reader = csv.reader(csvFile, delimiter = ';')
    for row in reader:
        print([row])
        messages.append(row)
        if row[4] != '':
            print(row)
messages = messages[1:]

now = datetime.datetime.now()
for i in range(len(messages)):
    timeObject = datetime.datetime.strptime(messages[i][1], '%d-%b-%y %I:%M %p')
    difference = now - timeObject
    print(difference)

print('Done!')